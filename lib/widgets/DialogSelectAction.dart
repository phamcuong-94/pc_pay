import 'dart:async';

import 'package:flutter/material.dart';

class DialogSelectAction {
  static showActionDialog({BuildContext context, List<Widget> widget }){
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding: EdgeInsets.all(20.0),
            children: widget,
          );
        });
  }
  static Future<bool> showConfirmDialog({BuildContext context, @required String title}) async {
    return await showDialog<bool>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: Text(title != null ? title : 'Bạn có muốn đăng xuất?'),
          contentPadding: EdgeInsets.all(10.0),
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FlatButton(
                  textColor: Colors.white,
                  color: Colors.red,
                  child: Text('Hủy'),
                  onPressed: (){
                    Navigator.pop(context, false);
                  },
                ),
                Padding(padding: EdgeInsets.only(right: 10.0),),
                FlatButton(
                  textColor: Colors.white,
                  color: Colors.green,
                  child: Text('OK'),
                  onPressed: (){
                    Navigator.pop(context, true);
                  },
                )
              ],
            ),
          ],
        );
      },
    );
  }
}