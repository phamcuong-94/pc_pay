import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:pc_pay/scoped_models/app_model.dart';
import 'package:pc_pay/screens/login/index.dart';
import './screens/khach_hang/home/index.dart';
import './screens/nhan_vien/home/index.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  State<StatefulWidget> createState() {
    return _MyHomePageState();
  }
}

class _MyHomePageState extends State<MyApp> {
  final AppModel _model = AppModel();
  bool _isAuthenticated = false;

  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return ScopedModel(
        model: _model,
        child: MaterialApp(
          theme: ThemeData(
              primaryColor: Colors.blue,
              accentColor: Colors.blue,
              scaffoldBackgroundColor: Colors.grey[300]
          ),
          debugShowCheckedModeBanner: false,
          title: "",
          routes: {
            '/': (BuildContext context)=>_isAuthenticated ? (_model.isCustomer ? HomePageKhachHangScreen() : HomePageNhanVienScreen()) : LoginScreen(_model),
            '/login': (BuildContext context)=>_isAuthenticated ? (_model.isCustomer ? HomePageKhachHangScreen() : HomePageNhanVienScreen()) : LoginScreen(_model),
            '/main-page': (BuildContext context)=>_isAuthenticated ? (_model.isCustomer ? HomePageKhachHangScreen() : HomePageNhanVienScreen()) : LoginScreen(_model),
          },
          onUnknownRoute: (RouteSettings settings) {
            return MaterialPageRoute(
                builder: (BuildContext context) =>
                (_model.isCustomer ? HomePageKhachHangScreen() : HomePageNhanVienScreen())
            );
          },
        )
    );
  }
}

