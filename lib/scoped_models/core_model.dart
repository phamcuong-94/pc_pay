import 'package:scoped_model/scoped_model.dart';

mixin CoreModel on Model{
  bool isLoadingData = true;
  bool isCustomer = false;
}