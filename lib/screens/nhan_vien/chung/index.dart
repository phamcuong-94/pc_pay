import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../thong_tin/index.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:pc_pay/scoped_models/app_model.dart';
import '../thong_ke/doanh_thu.dart';
import '../thong_ke/khach_hang_tiem_nang.dart';
import '../thong_ke/thong_ke_san_pham.dart';

class ChungScreen extends StatefulWidget {
  final String title = "Thông tin chung";
  final IconData icon = Icons.menu;

  @override
  State<StatefulWidget> createState() {
    return ChungScreenState();
  }
}

class ChungScreenState extends State<ChungScreen> {
  @override
  Widget build(BuildContext context) {
    Widget _inforContainer({List<Widget> widget}) {
      return Padding(
        padding: EdgeInsets.only(bottom: 20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(
                      top: BorderSide(color: Colors.grey[400]),
                      bottom: BorderSide(color: Colors.grey[400]))),
              child: Column(
                children: widget,
              ),
            ),
          ],
        ),
      );
    }

    Widget _itemForInforContainerRows(
        {String title, GestureTapCallback onTap, IconData icon}) {
      return Padding(
          padding: EdgeInsets.only(left: 8.0, bottom: 8.0, top: 8.0),
          child: GestureDetector(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Icon(icon, color: Colors.blue),
                Expanded(
                    child: Padding(
                  padding: EdgeInsets.only(left: 16.0),
                  child: Text(title, style: TextStyle(fontSize: 16.0)),
                ))
              ],
            ),
            onTap: onTap,
          ));
    }

    return ScopedModelDescendant<AppModel>(
        builder: (BuildContext context, Widget child, AppModel model) {
      return RefreshIndicator(
          child: ListView(
            children: <Widget>[
              _inforContainer(widget: [
                _itemForInforContainerRows(
                    icon: FontAwesomeIcons.userFriends,
                    title: "Thông tin cá nhân",
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ThongTinScreen()));
                    }),
                Divider(color: Colors.grey[400], indent: 8.0),
                Padding(
                    padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: GestureDetector(
                      child: Text("Đổi mật khẩu",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              color: Colors.blue),
                          textAlign: TextAlign.left),
                      onTap: () {
                        print("Chưa có chức năng này!");
                      },
                    ))
              ]),
              _inforContainer(widget: [
                Padding(
                    padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text("Thống kê",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16.0),
                        textAlign: TextAlign.left)),
                Divider(color: Colors.grey[400], indent: 8.0),
                _itemForInforContainerRows(
                    icon: FontAwesomeIcons.canadianMapleLeaf,
                    title: "Doanh thu",
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => DoanhThuScreen()));
                    }),
                _itemForInforContainerRows(
                    icon: FontAwesomeIcons.canadianMapleLeaf,
                    title: "Sản phẩm",
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ThongKeSanPhamScreen()));
                    }),
                _itemForInforContainerRows(
                    icon: FontAwesomeIcons.canadianMapleLeaf,
                    title: "Khách hàng tiềm năng",
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ThongKeKhachHangScreen()));
                    })
              ])
            ],
          ),
          onRefresh: () {});
    });
  }
}
