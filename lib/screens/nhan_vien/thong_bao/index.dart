import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../../widgets/CustomItemCard.dart';

class ThongBaoScreen extends StatefulWidget {
  final String title = "Thông báo";
  final IconData icon = FontAwesomeIcons.bell;
  @override
  State<StatefulWidget> createState() {
    return ThongBaoScreenState();
  }
}

class ThongBaoScreenState extends State<ThongBaoScreen> {
  @override
  Widget build(BuildContext context) {
    Widget _listThongBao() {
      return ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          Card(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Image.network("https://absoft.com.vn/wp-content/uploads/2018/05/38e7eb6699e55e777008cc8d691252c8.jpeg", fit: BoxFit.cover),
                Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        CustomItemCard.itemPadding(text: "Khuyến mãi",style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold)),
                        Text("Từ ngày 19/03/2019 đến ngày 35/19/201000 khi thanh toán bằng mã QRCode sẽ được cho luôn sản phẩm."),
                      ],
                    )
                )
              ],
            ),
          )
        ],
      );
    }
    return Center(
      child: _listThongBao(),
    );
  }
}
