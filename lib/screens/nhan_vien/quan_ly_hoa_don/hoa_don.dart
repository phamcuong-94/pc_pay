import 'package:flutter/material.dart';
import '../../../widgets/CustomItemCard.dart';
import '../../../widgets/DialogSelectAction.dart';

class ShowHoaDonScreen extends StatefulWidget {
  String idHoaDong;

  @override
  State<StatefulWidget> createState() {
    return ShowHoaDonScreenState();
  }
}

class ShowHoaDonScreenState extends State<ShowHoaDonScreen> {
  Widget _iconClose = Icon(Icons.close);
  bool _isHuy = false;
  @override
  Widget build(BuildContext context) {

    Widget _popupMenu = PopupMenuButton(
        itemBuilder: (BuildContext context){
          return <PopupMenuEntry>[
            PopupMenuItem(
                child: GestureDetector(
                  child: Text("Hủy hóa đơn"),
                  onTap: () async {
                    bool confirm = await DialogSelectAction.showConfirmDialog(
                        title: "Bạn muốn hủy hóa đơn này?",
                        context: context
                    );
                    setState(() {
                      if(confirm){
                        _isHuy = confirm;
                      }
                    });
                    Navigator.pop(context);
                  },
                )
            )
          ];
        }
    );

    return Scaffold(
      appBar: AppBar(
        title: Text("Chi tiết hóa đơn"),
        actions: <Widget>[
          _isHuy ? _iconClose : _popupMenu
        ],
      ),
      body: Center(
          child: ListView(
            children: <Widget>[
              Card(
                  child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          CustomItemCard.itemPadding(text: "Sản phẩm: Samsung Galaxy S10+"),
                          CustomItemCard.itemPadding(text: "Giá: 23.000.000"),
                          CustomItemCard.itemPadding(text: "Số lượng: 2"),
                          Row(
                            children: <Widget>[
                              CustomItemCard.itemPadding(text: "Thành tiền: "),
                              CustomItemCard.itemPadding(text: "46.000.000",style: TextStyle(color: Colors.red))
                            ],
                          )
                        ],
                      )))
            ],
          )
      ),
    );
  }
}
