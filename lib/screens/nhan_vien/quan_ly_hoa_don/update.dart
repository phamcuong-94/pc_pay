import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:pc_pay/scoped_models/app_model.dart';
import '../../../widgets/CustomInputWidgets.dart';
import '../../../widgets/DialogSelectAction.dart';

class UpdateHoaDonScreen extends StatefulWidget {
  @override
  _UpdateHoaDonScreenState createState() => _UpdateHoaDonScreenState();
}

class _UpdateHoaDonScreenState extends State<UpdateHoaDonScreen> {
  List _listSanPham = [];
  List _listSoLuong = [];
  String _soluong;
  String _sanpham;
  int _radioValue;

  final Map<String, dynamic> _formData = {
    'ma_hoa_don': null,
    'khach_hang': null,
    'tong_tien': null,
    'san_pham': null,
    'so_luong': null
  };

  List<Widget> _listChiTietHoaDon = [];
  TextEditingController _soluongController = TextEditingController();

  final _formKeyThongTinHoaDon = GlobalKey<FormState>();
  final _formKeyChiTietHoaDon = GlobalKey<FormState>();

  void _submitUpdate(AppModel model) async {
    final _formThongTinHoaDon = _formKeyThongTinHoaDon.currentState;
    final _formChiTietHoaDon = _formKeyChiTietHoaDon.currentState;
    if (_formThongTinHoaDon.validate() && _formChiTietHoaDon.validate()) {
      _formThongTinHoaDon.save();
      _formChiTietHoaDon.save();
      _listSanPham.add(_sanpham);
      _listSoLuong.add(_soluong);
      _soluong = null;
      _sanpham = null;
    }
  }

  Widget _formThongTinHoaDon(AppModel model) {
    return Padding(
      padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
        child: Form(
            key: _formKeyThongTinHoaDon,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                CustomUIWidget.inputDefaultValidator(
                    hintText: 'Mã hóa đơn',
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    obscureText: false,
                    onSaved: (input) => _formData["ma_hoa_don"] = input),
                DropdownButton(
                  isExpanded: true,
                  hint: Text("Khách hàng"),
                  value: _formData["khach_hang"],
                  items: <DropdownMenuItem>[
                    DropdownMenuItem(
                      child: Text("Trần Văn A"),
                      value: 0,
                    )
                  ].toList(),
                  onChanged: (value) {
                    setState(() {
                      _formData["khach_hang"] = value;
                    });
                  },
                ),
                CustomUIWidget.inputDefault(
                    hintText: 'Tổng tiền',
                    obscureText: false,
                    onSaved: (input) => _formData["tong_tien"] = input)
              ],
            )),
      ),
    );
  }

  updateListChiTietHoaDon({bool isDelete = false, int element}) {
    List<Widget> _tempListHoaDonUpdate = [];
    final _formChiTietHoaDon = _formKeyChiTietHoaDon.currentState;
    if (isDelete) {
      _listSanPham.removeAt(element);
      _listSoLuong.removeAt(element);
    } else {
      _formChiTietHoaDon.save();
      _listSanPham.add(_sanpham);
      _listSoLuong.add(_soluong);
      _soluong = null;
      _sanpham = null;
    }
    for (var i = 0; i < _listSanPham.length; i++) {
      _tempListHoaDonUpdate
          .add(_fillChiTietHoaDonWidget(_listSanPham[i], _listSoLuong[i], i));
    }
    setState(() {
      _soluongController.text = '';
      _listChiTietHoaDon = _tempListHoaDonUpdate;
    });
  }

  Widget _formChiTietHoaDon(AppModel model) {
    return Form(
        key: _formKeyChiTietHoaDon,
        child: Padding(
          padding: EdgeInsets.only(top: 10.0),
          child: Container(
            color: Colors.white,
            padding: EdgeInsets.only(left: 8.0, right: 8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                FlatButton(onPressed: (){
                  DialogSelectAction.showActionDialog(context: context,
                    widget: [
                      RadioListTile(title: Text("Trần Văn Bé"),value: 1, groupValue: _radioValue, onChanged: (index){setState(() {
                        _radioValue = index;
                      });}),
                      RadioListTile(title: Text("Âu Duon Phong"),value: 2, groupValue: _radioValue, onChanged: (index){setState(() {
                        _radioValue = index;
                      });})
                    ]
                  );
                }, child: Text("Bấm vào đây")),
                DropdownButton(
                  isExpanded: true,
                  hint: Text("Sản phẩm"),
                  value: _sanpham,
                  items: <DropdownMenuItem>[
                    DropdownMenuItem(
                      child: Text("SSD SAMSUNG 860 EVO 120GB"),
                      value: "SSD SAMSUNG 860 EVO 120GB",
                    )
                  ].toList(),
                  onChanged: (value) {
                    setState(() {
                      _sanpham = value;
                    });
                  },
                ),
                CustomUIWidget.inputDefault(
                    hintText: 'Số lượng',
                    obscureText: false,
                    controller: _soluongController,
                    onSaved: (input) => _soluong = input),
                IconButton(
                    icon: Icon(Icons.add),
                    onPressed: () {
                      updateListChiTietHoaDon();
                    })
              ],
            ),
          ),
        ));
  }

  Widget _fillChiTietHoaDonWidget(String sanpham, String soluong, int i) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(0.0),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 90,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Sản phẩn: $sanpham"),
                    Text("Số lượng: $soluong")
                  ],
                ),
              ),
              Expanded(
                flex: 10,
                child: IconButton(
                  alignment: Alignment.center,
                  icon: Icon(Icons.close),
                  onPressed: () {
                    updateListChiTietHoaDon(isDelete: true, element: i);
                  },
                ),
              )
            ],
          ),
        ),
        Divider(
          color: Colors.grey[200],
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant(
      builder: (BuildContext contex, Widget child, AppModel model) {
        return Scaffold(
          appBar: AppBar(title: Text("Thêm mới hóa đơn")),
          body: RefreshIndicator(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                      child: Center(
                        child: Text("Thông tin hóa đơn",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold)),
                      )),
                  _formThongTinHoaDon(model),
                  Padding(
                      padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                      child: Center(
                        child: Text("Chi tiết hóa đơn",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold)),
                      )),
                  Container(
                    color: Colors.white,
                    child: Column(
                      children: _listChiTietHoaDon,
                    ),
                  ),
                  _formChiTietHoaDon(model),
                  Padding(
                    padding: EdgeInsets.only(left: 8.0, right: 8.0),
                    child: CustomUIWidget.buttonExpanded(
                        hintText: "Lưu thông tin",
                        onPressed: () {
                          _submitUpdate(model);
                        }),
                  )
                ],
              ),
            ),
            onRefresh: () {},
          ),
        );
      },
    );
  }
}
