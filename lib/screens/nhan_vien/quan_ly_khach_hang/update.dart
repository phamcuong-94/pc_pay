import 'package:flutter/material.dart';
import '../../../widgets/CustomInputWidgets.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:pc_pay/scoped_models/app_model.dart';

class UpdateKhachHangScreen extends StatefulWidget {
  String id;
  UpdateKhachHangScreen(this.id);
  @override
  UpdateKhachHangScreenState createState() => UpdateKhachHangScreenState();
}

class UpdateKhachHangScreenState extends State<UpdateKhachHangScreen> {
  final Map<String, dynamic> _formData = {
    'ho_ten': null,
    'gioi_tinh': null,
    'ngay_sinh': "",
    'cmnd': null,
    'dia_chi': null,
  };
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  void _submitUpdate(AppModel model) async {
    final form = _formKey.currentState;

    if (form.validate()) {
      form.save();
    }
  }

  Widget _formUpdateKhachHang(AppModel model) {
    return SingleChildScrollView(
      padding: EdgeInsets.all(10.0),
      child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              CustomUIWidget.inputDefaultValidator(
                  hintText: 'Họ tên',
                  keyboardType: TextInputType.text,
                  initialValue: widget.id.isEmpty ? "" : widget.id,
                  textInputAction: TextInputAction.next,
                  obscureText: false,
                  onSaved: (input) => _formData["ho_ten"] = input),
              DropdownButton(
                isExpanded: true,
                hint: Text("Giới tính"),
                value: _formData["gioi_tinh"],
                items: <DropdownMenuItem>[
                  DropdownMenuItem(
                    child: Text("Nam"),
                    value: 0,
                  ),
                  DropdownMenuItem(
                    child: Text("Nữ"),
                    value: 1,
                  )
                ].toList(),
                onChanged: (value) {
                  setState(() {
                    _formData["gioi_tinh"] = value;
                  });
                },
              ),
              CustomUIWidget.inputDefault(
                  hintText: 'Ngày sinh',
                  keyboardType: TextInputType.datetime,
                  obscureText: false,
                  onSaved: (input) => _formData["ngay_sinh"] = input),
              CustomUIWidget.inputDefault(
                  hintText: 'CMND',
                  keyboardType: TextInputType.text,
                  obscureText: false,
                  onSaved: (input) => _formData["cmnd"] = input),
              CustomUIWidget.inputDefault(
                  hintText: 'Địa chỉ',
                  keyboardType: TextInputType.text,
                  obscureText: false,
                  onSaved: (input) => _formData["dia_chi"] = input),
              CustomUIWidget.buttonExpanded(
                  hintText: "Lưu thông tin",
                  onPressed: () {
                    _submitUpdate(model);
                  })
            ],
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<AppModel>(
      builder: (BuildContext context, Widget child, AppModel model) {
        return Scaffold(
          appBar: AppBar(title: Text("Cập nhật khách hàng")),
          body: RefreshIndicator(
            child: Container(
              color: Colors.white,
              constraints: BoxConstraints.expand(),
              child: _formUpdateKhachHang(model),
            ),
            onRefresh: () {},
          ),
        );
      },
    );
  }
}
