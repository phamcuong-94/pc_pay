import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../../widgets/CustomItemCard.dart';
import 'update.dart';

class KhachHangScreen extends StatefulWidget {
  final String title = "Khách hàng";
  final IconData icon = FontAwesomeIcons.user;
  @override
  State<StatefulWidget> createState() {
    return KhachHangScreenState();
  }
}

class KhachHangScreenState extends State<KhachHangScreen> {
  @override
  Widget build(BuildContext context) {
    Widget _listKhachHang() {
      return ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          GestureDetector(
            child: Card(
                child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            flex: 2,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                FittedBox(
                                    fit: BoxFit.contain,
                                    child: Padding(
                                        padding: EdgeInsets.only(bottom: 8.0),
                                        child: Text(
                                          "HĐ900001",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold, fontSize: 20.0),
                                        ))),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    CustomItemCard.itemPadding(text: "Ngày: 13/03/2019"),
                                    CustomItemCard.itemPadding(text: "Nhân viên: Trần Thị Huỳnh Như")
                                  ],
                                )
                              ],
                            )),
                        Expanded(
                            flex: 1,
                            child: FittedBox(
                                fit: BoxFit.contain,
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text("Tổng tiền",
                                        style: TextStyle(color: Colors.grey)),
                                    Text("10.000.000",
                                        style: TextStyle(color: Colors.red)),
                                    Text("Đã thanh toán",
                                        style: TextStyle(color: Colors.green)),
                                  ],
                                ))),
                      ],
                    ))),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context)=>UpdateKhachHangScreen("Đây là id")));
            },
          )
        ],
      );
    }
    return Center(
      child: _listKhachHang(),
    );
  }
}
