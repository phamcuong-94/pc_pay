import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pc_pay/scoped_models/app_model.dart';
import 'package:scoped_model/scoped_model.dart';
import '../../../widgets/CustomItemCard.dart';
import 'update.dart';

class QuanLyTheNapScreen extends StatefulWidget {
  final String title = "Thẻ nạp";
  final IconData icon = FontAwesomeIcons.creditCard;
  AppModel _model;

  QuanLyTheNapScreen(this._model);

  @override
  State<StatefulWidget> createState() {
    return QuanLyTheNapScreenState();
  }
}

class QuanLyTheNapScreenState extends State<QuanLyTheNapScreen> {

  @override
  Widget build(BuildContext context) {
    Widget _listTheNap(AppModel model) {
      return ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          GestureDetector(
            child: Card(
                child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    CustomItemCard.itemPadding(
                                        text: "Số seri: 53835832354"),
                                    CustomItemCard.itemPadding(
                                        text: "Mã thẻ: 4522330866328"),
                                    CustomItemCard.itemPadding(
                                        text: "Mệnh giá: 50.000")
                                  ],
                                )
                              ],
                            ))
                      ],
                    ))),
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (context)=>UpdateTheNapScreen("")));
              }
          )
        ],
      );
    }

    return ScopedModelDescendant<AppModel>(
      builder: (BuildContext context, Widget child, AppModel model) {
        return Center(
          child: _listTheNap(model),
        );
      },
    );
  }
}
