import 'package:flutter/material.dart';
import 'package:pc_pay/scoped_models/app_model.dart';
import 'package:scoped_model/scoped_model.dart';
import '../../../widgets/CustomInputWidgets.dart';

class UpdateTheNapScreen extends StatefulWidget {
  String id;
  UpdateTheNapScreen(this.id);
  @override
  State<StatefulWidget> createState() {
    return UpdateTheNapScreenState();
  }
}

class UpdateTheNapScreenState extends State<UpdateTheNapScreen> {
  final Map<String, dynamic> _formData = {
    'ma_the': null,
    'seri': null,
    'menh_gia': "",
  };
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  void _submitUpdate(AppModel model) async {
    final form = _formKey.currentState;

    if (form.validate()) {
      form.save();
    }
  }

  Widget _formUpdateTheNap(AppModel model) {
    return SingleChildScrollView(
      padding: EdgeInsets.all(10.0),
      child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              CustomUIWidget.inputDefaultValidator(
                  hintText: 'Mã thẻ',
                  keyboardType: TextInputType.text,
                  initialValue: widget.id.isEmpty ? "" : widget.id,
                  textInputAction: TextInputAction.next,
                  obscureText: false,
                  onSaved: (input) => _formData["ma_the"] = input),
              CustomUIWidget.inputDefaultValidator(
                  hintText: 'Số seri',
                  keyboardType: TextInputType.numberWithOptions(),
                  textInputAction: TextInputAction.next,
                  obscureText: false,
                  onSaved: (input) => _formData["seri"] = input),
              CustomUIWidget.inputDefaultValidator(
                  hintText: 'Mệnh giá',
                  keyboardType: TextInputType.numberWithOptions(),
                  obscureText: false,
                  onSaved: (input) => _formData["menh_gia"] = input),
              CustomUIWidget.buttonExpanded(
                  hintText: "Lưu thông tin",
                  onPressed: () {
                    _submitUpdate(model);
                  })
            ],
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<AppModel>(
      builder: (BuildContext context, Widget child, AppModel model) {
        return Scaffold(
          appBar: AppBar(
            title: Text("Cập nhật thẻ nạp"),
          ),
          body: RefreshIndicator(
            child: Container(
              color: Colors.white,
              constraints: BoxConstraints.expand(),
              child: _formUpdateTheNap(model),
            ),
            onRefresh: () {

            },
          ),
        );
      },
    );
  }
}
