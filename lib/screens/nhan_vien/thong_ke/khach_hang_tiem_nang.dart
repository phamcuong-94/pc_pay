import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pc_pay/scoped_models/app_model.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class ThongKeKhachHangScreen extends StatefulWidget {
  @override
  _ThongKeKhachHangScreenState createState() => _ThongKeKhachHangScreenState();
}

class _ThongKeKhachHangScreenState extends State<ThongKeKhachHangScreen> {
  final formatter = new NumberFormat("#,###");
  _cardChart(String title, Widget chart) {
    return Card(
        child: Column(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.all(10.0),
                child: Text(title,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 20))),
            chart
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    var dataKhachHang = [
      KhachHangTiemNang("Trần Nhựt Minh", 15000000),
      KhachHangTiemNang("Phạm Chí Cường", 18000000),
      KhachHangTiemNang("Trần Quang Dũng", 10500000),
      KhachHangTiemNang("Soobin Hoàng Sơn", 12000000),
      KhachHangTiemNang("Khá Bảnh", 10000000),
      KhachHangTiemNang("Trâm Anh", 15000000),
    ];

    var seriesKhachHang = [
      charts.Series(
          domainFn: (KhachHangTiemNang khachhangtiemnang, _) => khachhangtiemnang.hoTen,
          measureFn: (KhachHangTiemNang khachhangtiemnang, _) => khachhangtiemnang.tongTien,
          id: "KhachHangTiemNang",
          data: dataKhachHang,
          labelAccessorFn: (KhachHangTiemNang khachhangtiemnang, _) => '${khachhangtiemnang.hoTen}: ${formatter.format(khachhangtiemnang.tongTien)}'
      )
    ];

    var chartKhachHang = Padding(
        padding: EdgeInsets.all(8.0),
        child: SizedBox(
            height: 400,
            child: charts.BarChart(seriesKhachHang,
                animate: true,
                vertical: false,
                barRendererDecorator: charts.BarLabelDecorator<String>(),
                domainAxis: charts.OrdinalAxisSpec(renderSpec: charts.NoneRenderSpec())
            )));

    return ScopedModelDescendant<AppModel>(
        builder: (BuildContext context, Widget child, AppModel model) {
          return Scaffold(
                  appBar: AppBar(
                    title: Text("Thống kê doanh thu"),
                  ),
                  body: RefreshIndicator(
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            _cardChart("Khách hàng tiềm năng", chartKhachHang),
                          ],
                        ),
                      ),
                      onRefresh: (){}));
        });
  }
}

class KhachHangTiemNang{
  final String hoTen;
  final int tongTien;
  KhachHangTiemNang(this.hoTen,this.tongTien);
}
