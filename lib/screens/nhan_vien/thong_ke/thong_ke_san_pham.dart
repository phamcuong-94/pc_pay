import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pc_pay/scoped_models/app_model.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class ThongKeSanPhamScreen extends StatefulWidget {
  @override
  _ThongKeSanPhamScreenState createState() => _ThongKeSanPhamScreenState();
}

class _ThongKeSanPhamScreenState extends State<ThongKeSanPhamScreen> {
  final formatter = new NumberFormat("#.###");

  _cardChart(String title, Widget chart) {
    return Card(
        child: Column(
      children: <Widget>[
        Padding(
            padding: EdgeInsets.all(10.0),
            child: Text(title,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20))),
        chart
      ],
    ));
  }

  @override
  Widget build(BuildContext context) {
    var dataKhachHang = [
      TopSanPham("SSD SamSung 860 EVO", 5),
      TopSanPham("SSD KingStone", 2),
      TopSanPham("Laptop Gaming Asus", 1),
    ];

    var seriesTopSanPham = [
      charts.Series(
        domainFn: (TopSanPham topsanpham, _) => topsanpham.ten_sp,
        measureFn: (TopSanPham topsanpham, _) => topsanpham.so_luong_da_ban,
        id: "TopSanPham",
        data: dataKhachHang,
      )
    ];

    var chartTopSanPham = Padding(
        padding: EdgeInsets.all(8.0),
        child: SizedBox(
            height: 400,
            child: charts.PieChart(seriesTopSanPham, animate: true, behaviors: [
              new charts.DatumLegend(
                position: charts.BehaviorPosition.bottom,
                horizontalFirst: false,
                cellPadding: new EdgeInsets.only(right: 4.0, bottom: 0.0),
                legendDefaultMeasure: charts.LegendDefaultMeasure.firstValue,
                showMeasures: true,
              ),
            ])));

    return ScopedModelDescendant<AppModel>(
        builder: (BuildContext context, Widget child, AppModel model) {
      return Scaffold(
          appBar: AppBar(
            title: Text("Thống kê doanh thu"),
          ),
          body: RefreshIndicator(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    _cardChart("Sản phẩm bán nhiều nhất", chartTopSanPham),
                    Card(
                      child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Container(
                              height: 400,
                              child: ListView(
                                children: <Widget>[
                                  Text("Tên sản phẩm: SSD SamSung 860 EVO plus 120GB"),
                                  Text("Giá: ${formatter.format(2000000)}"),
                                  Text("Số lượng: 100"),
                                  Text("Đã bán: 6"),
                                  Text("Còn lại: 94"),
                                  Divider(color: Colors.grey[400]),
                                ],
                              )),
                      ),
                    )
                  ],
                ),
              ),
              onRefresh: () {}));
    });
  }
}

class TopSanPham {
  final String ten_sp;
  final int so_luong_da_ban;

  TopSanPham(this.ten_sp, this.so_luong_da_ban);
}
