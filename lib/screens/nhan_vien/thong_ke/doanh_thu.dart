import 'package:flutter/material.dart';
import 'package:pc_pay/scoped_models/app_model.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:intl/intl.dart';

class DoanhThuScreen extends StatefulWidget {
  /*final List<charts.Series> seriesList;
  final bool animate;
  DoanhThuScreen(this.seriesList,this.animate);*/
  @override
  _DoanhThuScreenState createState() => _DoanhThuScreenState();
}

class _DoanhThuScreenState extends State<DoanhThuScreen> {
  final formatter = new NumberFormat("#,###");
  _cardChart(String title, Widget chart) {
    return Card(
        child: Column(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.all(10.0),
                child: Text(title,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 20))),
            chart
      ],
    ));
  }

  @override
  Widget build(BuildContext context) {
    var data = [
      DoanhThuThang(1, 50000),
      DoanhThuThang(2, 30000),
      DoanhThuThang(3, 100000),
      DoanhThuThang(4, 300000),
      DoanhThuThang(5, 400000),
      DoanhThuThang(6, 450000),
      DoanhThuThang(7, 400000),
      DoanhThuThang(8, 300000),
      DoanhThuThang(9, 350000),
      DoanhThuThang(10, 200000),
      DoanhThuThang(11, 500000),
      DoanhThuThang(12, 1000000),
    ];

    var series = [
      charts.Series(
          domainFn: (DoanhThuThang doanhthuthang, _) => doanhthuthang.month,
          measureFn: (DoanhThuThang doanhthuthang, _) => doanhthuthang.revenue,
          id: "DoanhThuThang",
          data: data,
          labelAccessorFn: (DoanhThuThang doanhthuthang, _) => '${formatter.format(doanhthuthang.revenue)}'
      )
    ];

    var chartWidget = Padding(
        padding: EdgeInsets.all(8.0),
        child: SizedBox(
            height: 400,
            child: charts.LineChart(series,
                animate: true,
                defaultRenderer:
                    charts.LineRendererConfig(includePoints: true))));

    return ScopedModelDescendant<AppModel>(
        builder: (BuildContext context, Widget child, AppModel model) {
      return Scaffold(
              appBar: AppBar(
                title: Text("Thống kê doanh thu"),
              ),
              body: RefreshIndicator(
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        _cardChart("Doanh thu theo tháng", chartWidget),
                        _cardChart("Doanh thu theo năm", chartWidget)
                      ],
                    ),
                  ),
                  onRefresh: (){}
              )
      );
    });
  }
}

class DoanhThuThang {
  final int month;
  final int revenue;

  DoanhThuThang(this.month, this.revenue);
}