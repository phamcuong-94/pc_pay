import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pc_pay/scoped_models/app_model.dart';

class TrangChuNhanVienScreen extends StatefulWidget {
  final String title = "Trang chủ";
  final IconData icon = Icons.home;
  AppModel _model;

  TrangChuNhanVienScreen(this._model);

  @override
  State<StatefulWidget> createState() {
    return TrangChuNhanVienScreenState();
  }
}

class TrangChuNhanVienScreenState extends State<TrangChuNhanVienScreen> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget _buildTrangChu() {
      return SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[Text("Trang chủ chưa có gì hết!")],
          ),
        ),
      );
    }

    return Center(
      child: _buildTrangChu(),
    );
  }
}
