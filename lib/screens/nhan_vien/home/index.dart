import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pc_pay/scoped_models/app_model.dart';
import '../quan_ly_hoa_don/index.dart';
import '../quan_ly_hoa_don/update.dart';
import 'home.dart';
import '../quan_ly_khach_hang/index.dart';
import '../quan_ly_khach_hang/update.dart';
import '../quan_ly_the_nap/index.dart';
import '../quan_ly_the_nap/update.dart';
import '../chung/index.dart';
import 'package:scoped_model/scoped_model.dart';

class HomePageNhanVienScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageNhanVienScreenState();
  }
}

class HomePageNhanVienScreenState extends State<HomePageNhanVienScreen> {
  PageController _pageController;
  int _selectedIndex = 0;

  List<Widget> _listWidget = [
    IconButton(
        icon: Icon(FontAwesomeIcons.qrcode),
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        onPressed: () {
          print("GG");
        })
  ];
  bool _centerTitle = false;
  Widget _appBarTiltle = Text("Trang chủ");
  Widget _appBar(AppModel model) {
    return AppBar(
      backgroundColor: Colors.blue,
      title: _appBarTiltle,
      centerTitle: _centerTitle,
      actions: _listWidget,
    );
  }

  @override
  void initState() {
    _pageController = new PageController(initialPage: _selectedIndex);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<AppModel>(
      builder: (BuildContext context, Widget child, AppModel model) {
        final bottomNavagationBarItems = [
          {
            "title": TrangChuNhanVienScreen(model).title,
            "icon": TrangChuNhanVienScreen(model).icon
          },
          {
            "title": QuanLyHoaDonScreen(model).title,
            "icon": QuanLyHoaDonScreen(model).icon
          },
          {"title": KhachHangScreen().title, "icon": KhachHangScreen().icon},
          {
            "title": QuanLyTheNapScreen(model).title,
            "icon": QuanLyTheNapScreen(model).icon
          },
          {"title": "Chung", "icon": ChungScreen().icon},
        ];

        _bottomNavagationBarOptions() {
          List<BottomNavigationBarItem> bottomNavagationBarOptions = [];
          for (var i = 0; i < bottomNavagationBarItems.length; i++) {
            var bottomItem = bottomNavagationBarItems[i];
            bottomNavagationBarOptions.add(BottomNavigationBarItem(
                icon: Icon(bottomItem["icon"]),
                title: Text(bottomItem["title"])));
          }
          return bottomNavagationBarOptions;
        }

        List<Widget> listPage = [
          TrangChuNhanVienScreen(model),
          QuanLyHoaDonScreen(model),
          KhachHangScreen(),
          QuanLyTheNapScreen(model),
          ChungScreen()
        ];

        return Scaffold(
            appBar: _appBar(model),
            bottomNavigationBar: BottomNavigationBar(
              items: _bottomNavagationBarOptions(),
              currentIndex: _selectedIndex,
              showSelectedLabels: true,
              showUnselectedLabels: false,
              selectedItemColor: Colors.blue,
              unselectedItemColor: Colors.grey,
              type: BottomNavigationBarType.shifting,
              onTap: (index) {
                setState(() {
                  _pageController.animateToPage(index,
                      duration: Duration(milliseconds: 1),
                      curve: Curves.linear);
                  _selectedIndex = index;
                });
              },
            ),
            body: PageView(
              children: listPage,
              controller: _pageController,
              onPageChanged: (newPageIndex) {
                onPageChanged(newPageIndex, model);
                setState(() {
                  _selectedIndex = newPageIndex;
                });
              },
            ));
      },
    );
  }

  void onPageChanged(int page, AppModel model) {
    navigateToPage(page);
    switch (page) {
      case 0:
        setState(() {
          _appBarTiltle = Text(TrangChuNhanVienScreen(model).title);
          _listWidget = [
            IconButton(
                icon: Icon(FontAwesomeIcons.qrcode),
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () {})
          ];
          _centerTitle = false;
        });
        break;
      case 1:
        setState(() {
          _appBarTiltle = Text(QuanLyHoaDonScreen(model).title);
          _listWidget = [
            IconButton(
                icon: Icon(Icons.search),
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () {
                  setState(() {
                    _appBarTiltle = AnimatedContainer(
                      duration: Duration(
                        seconds: 2,
                      ),
                      curve: Curves.decelerate,
                      child: TextFormField(
                          autofocus: true,
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                              hintText: "Tìm hóa đơn...",
                              hintStyle: TextStyle(color: Colors.white),
                              border: InputBorder.none,
                              focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey[300])))),
                    );
                  });
                }),
            IconButton(
                icon: Icon(Icons.add),
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => UpdateHoaDonScreen()));
                })
          ];
          _centerTitle = false;
        });
        break;
      case 2:
        setState(() {
          _appBarTiltle = Text(KhachHangScreen().title);
          _listWidget = [
            IconButton(
                icon: Icon(Icons.add),
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => UpdateKhachHangScreen("")));
                })
          ];
          _centerTitle = true;
        });
        break;
      case 3:
        _appBarTiltle = Text(QuanLyTheNapScreen(model).title);
        _listWidget = [
          IconButton(
              icon: Icon(Icons.search),
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onPressed: () {
                setState(() {
                  _appBarTiltle = TextFormField(
                      autofocus: true,
                      style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                          hintText: "Tìm hóa đơn...",
                          hintStyle: TextStyle(color: Colors.white),
                          border: InputBorder.none,
                          focusedBorder: UnderlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.grey[300]))));
                });
              }),
          IconButton(
              icon: Icon(Icons.add),
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => UpdateTheNapScreen("")));
              })
        ];
        _centerTitle = false;
        break;
      case 4:
        _appBarTiltle = Text(ChungScreen().title);
        _listWidget = [];
        _centerTitle = true;
        break;
    }
  }

  void navigateToPage(int page) {
    _pageController.animateToPage(page,
        duration: Duration(milliseconds: 300), curve: Curves.linear);
  }
}
