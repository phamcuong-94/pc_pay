import 'package:flutter/material.dart';
import 'package:pc_pay/scoped_models/app_model.dart';
import '../../widgets/CustomInputWidgets.dart';
import 'package:scoped_model/scoped_model.dart';
import 'NhanVienLogin.dart';
import 'KhachHangLogin.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class LoginScreen extends StatefulWidget {
  AppModel _model;
  LoginScreen(this._model);
  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> {
  PageController _pageController;
  int _selectedIndex = 1;

  @override
  void initState() {
    _pageController = new PageController(initialPage: _selectedIndex);
    super.initState();
  }

  final _formKey = GlobalKey<FormState>();
  void _submit() async {
    print(widget._model.isCustomer.toString());
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      Navigator.pushNamed(context, "/home-page");
    }
  }

  Widget _loginHomeScreen() {
    final screenSize = MediaQuery.of(context).size;
    return Center(
        child: SingleChildScrollView(
            child: Form(
                key: _formKey,
                child: Container(
                    padding: EdgeInsets.only(left: 30.0, right: 30.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image(
                          image: AssetImage('images/logo.png'),
                          width: (screenSize.width / 2),
                          height: (screenSize.width / 2),
                        ),
                        Padding(
                          padding:
                          EdgeInsets.only(bottom: 10.0, top: 30.0),
                          child: CustomUIWidget.inputBorderRadius(
                              hintText: "Tài khoản",
                              color:
                              Color.fromRGBO(242, 242, 242, 50.0),
                              icon: Icon(FontAwesomeIcons.user)),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 10.0),
                          child: CustomUIWidget.inputBorderRadius(
                              hintText: "Mật khẩu",
                              color:
                              Color.fromRGBO(242, 242, 242, 50.0),
                              icon: Icon(FontAwesomeIcons.lock),
                              obscureText: true),
                        ),
                        Row(
                          children: <Widget>[
                            Checkbox(value:  widget._model.isCustomer, onChanged: (checked){
                              setState(() {
                                print(checked);
                                widget._model.isCustomer = checked;
                              });
                            }),
                            Text("Đăng nhập với tư cách là khách hàng")
                          ],
                        ),
                        Row(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                                child: Padding(
                                  padding: EdgeInsets.only(right: 5.0),
                                  child: CustomUIWidget
                                      .buttonBorderRadius(
                                      hintText: "Đăng nhập",
                                      color: Colors.blue,
                                      onPressed: () {
                                        _submit();
                                      }),
                                ))
                          ],
                        )
                      ],
                    )))
        ));
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<AppModel>(
      builder: (BuildContext context, Widget child, AppModel model) {

        List<Widget> listPageLogin = [
          KhachHangLoginScreen(model),
          _loginHomeScreen(),
          NhanVienLoginScreen(model)
        ];

        return Scaffold(
            backgroundColor: Colors.white,
            body: PageView(
              children: listPageLogin,
              controller: _pageController,
              onPageChanged: (newPageIndex) {
                _pageController.animateToPage(newPageIndex,
                    duration: Duration(milliseconds: 300),
                    curve: Curves.easeInOut);
                setState(() {
                  _selectedIndex = newPageIndex;
                });
              },
            ));
      },
    );
  }
  void navigateToPage(int page) {
    _pageController.animateToPage(page,
        duration: Duration(milliseconds: 500), curve: (Curves.easeInCubic));
  }
}


