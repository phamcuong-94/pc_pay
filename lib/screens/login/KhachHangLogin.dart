import 'package:flutter/material.dart';
import 'package:pc_pay/scoped_models/app_model.dart';
import '../../widgets/CustomInputWidgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../khach_hang/home/index.dart';

class KhachHangLoginScreen extends StatefulWidget{
  AppModel _model;
  KhachHangLoginScreen(this._model);
  @override
  State<StatefulWidget> createState() {
    return KhachHangLoginScreenState();
  }
}

class KhachHangLoginScreenState extends State<KhachHangLoginScreen> with SingleTickerProviderStateMixin{

  @override
  void initState() {
    widget._model.isCustomer = true;
    super.initState();
  }

  final _formKey = GlobalKey<FormState>();
  void _submit() async {
    print(widget._model.isCustomer.toString());
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      Navigator.pushNamed(context, "/home-page");
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Center(
        child: SingleChildScrollView(
            child: Form(
                key: _formKey,
                child: Container(
                    padding: EdgeInsets.only(left: 30.0, right: 30.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image(
                          image: AssetImage('images/logo.png'),
                          width: (screenSize.width / 2),
                          height: (screenSize.width / 2),
                        ),
                        Padding(
                          padding:
                          EdgeInsets.only(bottom: 10.0, top: 30.0),
                          child: CustomUIWidget.inputBorderRadius(
                              hintText: "Tài khoản",
                              color:
                              Color.fromRGBO(242, 242, 242, 50.0),
                              icon: Icon(FontAwesomeIcons.user)),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 10.0),
                          child: CustomUIWidget.inputBorderRadius(
                              hintText: "Mật khẩu",
                              color:
                              Color.fromRGBO(242, 242, 242, 50.0),
                              icon: Icon(FontAwesomeIcons.lock),
                              obscureText: true),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(bottom: 10.0),
                              child: GestureDetector(
                                child: Text("Quên mật khẩu",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.blue)),
                                onTap: () {
                                  print("Quên mật khẩu!");
                                },
                              ),
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                                flex: 2,
                                child: Padding(
                                  padding: EdgeInsets.only(right: 5.0),
                                  child: CustomUIWidget
                                      .buttonBorderRadius(
                                      hintText: "Đăng nhập KH",
                                      color: Colors.blue,
                                      onPressed: () {
                                        _submit();
                                      }),
                                )),
                            Expanded(
                                flex: 1,
                                child: Padding(
                                  padding: EdgeInsets.only(left: 5.0),
                                  child: CustomUIWidget
                                      .buttonBorderRadius(
                                      hintText: "Đăng ký",
                                      color: Colors.green,
                                      onPressed: () {
                                        print("Đăng ký");
                                      }),
                                ))
                          ],
                        )
                      ],
                    )))
        ));
  }
}