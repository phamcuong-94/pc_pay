import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:pc_pay/scoped_models/app_model.dart';
import 'package:image_picker/image_picker.dart';
import 'update.dart';
import 'qr_thong_tin.dart';
import '../the_nap/nap_the.dart';

class ThongTinScreen extends StatefulWidget {
  final String title = "Thông tin cá nhân";
  final IconData icon = FontAwesomeIcons.user;

  @override
  State<StatefulWidget> createState() {
    return ThongTinScreenState();
  }
}

class ThongTinScreenState extends State<ThongTinScreen> {
  File _image;

  Future getImageGallery() async {
    var imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = imageFile;
    });
  }

  Future getImageCamera() async {
    var imageFile = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      _image = imageFile;
    });
  }

  void _showSelectionDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: Text("Ảnh đại diện",
                style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
            contentPadding: EdgeInsets.all(20.0),
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(bottom: 10.0),
                child: GestureDetector(
                  child: Text("Chụp ảnh mới", style: TextStyle(fontSize: 16.0)),
                  onTap: () {
                    getImageCamera();
                    Navigator.pop(context);
                  },
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(top: 10.0),
                  child: GestureDetector(
                    child: Text("Chọn ảnh có sẵn",
                        style: TextStyle(fontSize: 16.0)),
                    onTap: () {
                      getImageGallery();
                      Navigator.pop(context);
                    },
                  )),
            ],
          );
        });
  }

  Widget _selectAvatar({Image image}) {
    return Container(
        decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(20.0)),
        child: Stack(
          alignment: Alignment(1.0, 1.0),
          children: <Widget>[
            ClipRRect(borderRadius: BorderRadius.circular(20.0), child: image),
            Icon(
              FontAwesomeIcons.plusCircle,
              color: Colors.blue,
            )
          ],
        ));
  }

  Widget _buildSelectAvatar(AppModel model) {
    return Center(
        child: Padding(
      padding: EdgeInsets.only(bottom: 30.0, top: 10.0),
      child: Container(
        width: 120.0,
        height: 120.0,
        child: InkWell(
          child: _image == null
              ? _selectAvatar(
                  image: Image.asset("images/logo.png",
                      fit: BoxFit.fill, width: 120.0, height: 120.0))
              : _selectAvatar(
                  image: Image.file(_image,
                      fit: BoxFit.cover, width: 120.0, height: 120.0)),
          onTap: _showSelectionDialog,
        ),
      ),
    ));
  }

  Widget _inforContainer({List<Widget> widget}) {
    return Padding(
      padding: EdgeInsets.only(bottom: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border(
                    top: BorderSide(color: Colors.grey[400]),
                    bottom: BorderSide(color: Colors.grey[400]))),
            child: Column(
              children: widget,
            ),
          ),
        ],
      ),
    );
  }

  Widget _qrCode(String id) {
    return GestureDetector(
      child: Padding(
          padding: EdgeInsets.only(bottom: 20.0),
          child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(
                      top: BorderSide(color: Colors.grey[400]),
                      bottom: BorderSide(color: Colors.grey[400]))),
              child: Column(
                children: <Widget>[
                  QrImage(
                    data: '{"id":$id}',
                    size: 200,
                    gapless: true,
                  ),
                  Text(
                    "Quét mã QR để thanh toán",
                    style: TextStyle(color: Colors.grey),
                  )
                ],
              ))),
      onTap: () {
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => QRThongTinScreen(id)));
      },
    );
  }

  Widget _itemForInforContainerRows(
      {String title,
      String value,
      bool disable = false,
      bool isBalance = false,
      Color textColor = Colors.black,
      GestureTapCallback onTap}) {
    return Padding(
        padding: EdgeInsets.only(left: 8.0, bottom: 8.0, top: 8.0),
        child: GestureDetector(
          child: Row(
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Text(title, style: TextStyle(fontSize: 16.0))),
              Expanded(
                  flex: 2,
                  child: Text(value,
                      style: TextStyle(fontSize: 16.0, color: textColor),
                      textAlign: TextAlign.right)),
              disable
                  ? Icon(Icons.arrow_forward_ios, color: Colors.transparent)
                  : Icon(Icons.arrow_forward_ios, color: Colors.grey)
            ],
          ),
          onTap: () {
            print(onTap);
            // ignore: unnecessary_statements
            if (!disable) {
              if (isBalance) {
                return Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => NapTheScreen()));
              }else{
                return Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => UpdateThongTinScreen(title, value)));
              }
            }
          },
        ));
  }

  @override
  Widget build(BuildContext context) {
    Widget _listThongTin(AppModel model) {
      return ListView(
        children: <Widget>[
          _buildSelectAvatar(model),
          _inforContainer(widget: [
            _itemForInforContainerRows(
                title: "Số tài khoản", value: "147856987452", disable: true),
            _itemForInforContainerRows(
                title: "Số dư",
                value: "500000",
                textColor: Colors.red,
                isBalance: true),
            _itemForInforContainerRows(
                title: "Tài khoản", value: "cuongtc2010"),
            Divider(color: Colors.grey[400], indent: 8.0),
            Padding(
                padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
                child: GestureDetector(
                  child: Text("Đổi mật khẩu",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                          color: Colors.blue),
                      textAlign: TextAlign.left),
                  onTap: () {
                    print("Chưa có chức năng này!");
                  },
                ))
          ]),
          _inforContainer(widget: [
            _itemForInforContainerRows(
                title: "Họ tên", value: "Phạm Chí Cường"),
            Divider(color: Colors.grey[400], indent: 8.0),
            _itemForInforContainerRows(title: "Giới tính", value: "Nữ"),
            Divider(color: Colors.grey[400], indent: 8.0),
            _itemForInforContainerRows(title: "Ngày sinh", value: "24/09/1994"),
            Divider(color: Colors.grey[400], indent: 8.0),
            _itemForInforContainerRows(title: "CMND", value: "352173613"),
            Divider(color: Colors.grey[400], indent: 8.0),
            _itemForInforContainerRows(
                title: "Địa chỉ",
                value: "445/3 Long Thạnh B, Long Thạnh, Tân Châu, An Giang")
          ]),
          _qrCode("Cái này sau sẽ là ID")
        ],
      );
    }

    return ScopedModelDescendant<AppModel>(
      builder: (BuildContext context, Widget child, AppModel model) {
        return RefreshIndicator(
            child: Center(
              child: _listThongTin(model),
            ),
            onRefresh: () {
              print("GG");
            });
      },
    );
  }
}
