import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QRThongTinScreen extends StatefulWidget {
  String id;
  QRThongTinScreen(this.id);
  @override
  State<StatefulWidget> createState() {
    return QRThongTinScreenState();
  }
}

class QRThongTinScreenState extends State<QRThongTinScreen> {
  String _id;
  @override
  void initState() {
    _id = widget.id;
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Container(
            color: Colors.white,
            child: Center(
                child: QrImage(
                  data: '{"id":$_id}',
                  size: 300,
                  gapless: true,
                )),
          )
      )
    );
  }
}
