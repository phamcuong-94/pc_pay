import 'package:flutter/material.dart';

class UpdateThongTinScreen extends StatefulWidget {
  String title;
  String value;

  UpdateThongTinScreen(this.title, this.value);

  @override
  State<StatefulWidget> createState() {
    return UpdateThongTinScreenState();
  }
}

class UpdateThongTinScreenState extends State<UpdateThongTinScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          centerTitle: true,
          title: Text(widget.title),
        ),
        body: Padding(
            padding: EdgeInsets.only(top: 20.0),
            child: Column(
              children: <Widget>[
                Container(
                    padding: EdgeInsets.all(8.0),
                    color: Colors.white,
                    child: Form(
                        child: TextFormField(
                      decoration: InputDecoration(border: InputBorder.none),
                      autofocus: true,
                      initialValue: widget.value,
                    ))),
                Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: FlatButton(
                          color: Colors.blue,
                          child: Text("Lưu thông tin", style: TextStyle(color: Colors.white),),
                          onPressed: () {
                            print(widget.value);
                          },
                        ))
                      ],
                    ))
              ],
            )));
  }
}
