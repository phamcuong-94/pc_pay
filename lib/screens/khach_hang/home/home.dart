import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TrangChuScreen extends StatelessWidget{
  final String title = "Trang chủ";
  final IconData icon = Icons.home;
  final AppBar appBar = AppBar(
    backgroundColor: Colors.blue,
    title: Text(
      "Trang chủ",
      style: TextStyle(fontSize: 16.0),
    ),
    actions: <Widget>[
      Padding(
          padding: EdgeInsets.only(right: 10.0),
          child: IconButton(
              icon: Icon(FontAwesomeIcons.qrcode),
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onPressed: () {
                print("GG");
              }))
    ],
  );

  @override
  Widget build(BuildContext context) {
    Widget _buildTrangChu(){
      return SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text("Trang chủ chưa có gì hết!")
            ],
          ),
        ),
      );
    }
    return Center(
      child: _buildTrangChu(),
    );
  }
}