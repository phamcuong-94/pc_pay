import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pc_pay/scoped_models/app_model.dart';
import '../hoa_don/index.dart';
import 'home.dart';
import '../thong_bao/index.dart';
import '../the_nap/index.dart';
import '../thong_tin/index.dart';
import 'package:scoped_model/scoped_model.dart';

class HomePageKhachHangScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageKhachHangScreenState();
  }
}

class HomePageKhachHangScreenState extends State<HomePageKhachHangScreen> {
  PageController _pageController;
  int _selectedIndex = 0;
  Widget _appBarTiltle = Text(TrangChuScreen().title);
  List<Widget> _listWidget = [
    IconButton(
        icon: Icon(FontAwesomeIcons.qrcode),
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        onPressed: () {
          print("GG");
        })
  ];
  bool _centerTitle = false;

  Widget _appBar() {
    return AppBar(
      backgroundColor: Colors.blue,
      title: _appBarTiltle,
      centerTitle: _centerTitle,
      actions: _listWidget,
    );
  }

  @override
  void initState() {
    _pageController = new PageController(initialPage: _selectedIndex);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<AppModel>(
      builder: (BuildContext context, Widget child, AppModel model) {
        final bottomNavagationBarItems = [
          {"title": TrangChuScreen().title, "icon": TrangChuScreen().icon},
          {"title": HoaDonScreen().title, "icon": HoaDonScreen().icon},
          {"title": ThongBaoScreen().title, "icon": ThongBaoScreen().icon},
          {"title": TheNapScreen().title, "icon": TheNapScreen().icon},
          {"title": "Tôi", "icon": ThongTinScreen().icon},
        ];

        _bottomNavagationBarOptions() {
          List<BottomNavigationBarItem> bottomNavagationBarOptions = [];
          for (var i = 0; i < bottomNavagationBarItems.length; i++) {
            var bottomItem = bottomNavagationBarItems[i];
            bottomNavagationBarOptions.add(BottomNavigationBarItem(
                icon: Icon(bottomItem["icon"]),
                title: Text(bottomItem["title"])));
          }
          return bottomNavagationBarOptions;
        }

        List<Widget> listPage = [
          TrangChuScreen(),
          HoaDonScreen(),
          ThongBaoScreen(),
          TheNapScreen(),
          ThongTinScreen()
        ];

        return Scaffold(
            appBar: _appBar(),
            bottomNavigationBar: BottomNavigationBar(
              items: _bottomNavagationBarOptions(),
              currentIndex: _selectedIndex,
              showSelectedLabels: true,
              showUnselectedLabels: false,
              selectedItemColor: Colors.blue,
              unselectedItemColor: Colors.grey,
              type: BottomNavigationBarType.shifting,
              onTap: (index) {
                setState(() {
                  _pageController.animateToPage(index,
                      duration: Duration(milliseconds: 1),
                      curve: Curves.easeInOut);
                  _selectedIndex = index;
                });
              },
            ),
            body: PageView(
              children: listPage,
              controller: _pageController,
              onPageChanged: (newPageIndex) {
                onPageChanged(newPageIndex);
                setState(() {
                  _selectedIndex = newPageIndex;
                });
              },
            ));
      },
    );
  }

  void onPageChanged(int page) {
    switch (page) {
      case 0:
        setState(() {
          _appBarTiltle = Text(TrangChuScreen().title);
          _listWidget = [
            IconButton(
                icon: Icon(FontAwesomeIcons.qrcode),
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () {

                })
          ];
          _centerTitle = false;
        });
        break;
      case 1:
        setState(() {
          _appBarTiltle = Text(HoaDonScreen().title);
          _listWidget = [
            IconButton(
                icon: Icon(Icons.search),
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () {
                  setState(() {
                    _appBarTiltle = TextFormField(
                        autofocus: true,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            hintText: "Tìm hóa đơn...",
                            hintStyle: TextStyle(color: Colors.white),
                            border: InputBorder.none,
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey[300])
                            )
                        )
                    );
                  });
                })
          ];
          _centerTitle = false;
        });
        break;
      case 2:
        setState(() {
          _appBarTiltle = Text(ThongBaoScreen().title);
          _listWidget = [];
          _centerTitle = true;
        });
        break;
      case 3:
        _appBarTiltle = Text(TheNapScreen().title);
        _listWidget = [];
        _centerTitle = true;
        break;
      case 4:
        _appBarTiltle = Text(ThongTinScreen().title);
        _listWidget = [];
        _centerTitle = true;
        break;
    }
  }

  void navigateToPage(int page) {
    _pageController.animateToPage(page,
        duration: Duration(milliseconds: 300), curve: Curves.easeInOut);
  }
}
