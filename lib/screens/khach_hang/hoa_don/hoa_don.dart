import 'package:flutter/material.dart';
import '../../../widgets/CustomItemCard.dart';

class ShowHoaDonScreen extends StatefulWidget {
  String idHoaDong;
  @override
  State<StatefulWidget> createState() {
    return ShowHoaDonScreenState();
  }
}

class ShowHoaDonScreenState extends State<ShowHoaDonScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Chi tiết hóa đơn HĐ900001"),
      ),
      body: Center(
          child: ListView(
            children: <Widget>[
              Card(
                  child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          CustomItemCard.itemPadding(text: "Sản phẩm: Samsung Galaxy S10+"),
                          CustomItemCard.itemPadding(text: "Giá: 23.000.000"),
                          CustomItemCard.itemPadding(text: "Số lượng: 2"),
                          Row(
                            children: <Widget>[
                              CustomItemCard.itemPadding(text: "Thành tiền: "),
                              CustomItemCard.itemPadding(text: "46.000.000",style: TextStyle(color: Colors.red))
                            ],
                          )
                        ],
                      )))
            ],
          )
      ),
    );
  }
}
