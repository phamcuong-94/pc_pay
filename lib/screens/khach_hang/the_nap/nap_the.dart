import 'package:flutter/material.dart';

class NapTheScreen extends StatefulWidget {
  @override
  _NapTheScreenState createState() => _NapTheScreenState();
}

class _NapTheScreenState extends State<NapTheScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          centerTitle: true,
          title: Text("Nạp tiền vào tài khoản"),
        ),
        body: Padding(
            padding: EdgeInsets.only(top: 20.0),
            child: Column(
              children: <Widget>[
                Container(
                    padding: EdgeInsets.all(8.0),
                    color: Colors.white,
                    child: Form(
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(border: InputBorder.none, hintText: "Nhập số seri để nạp tiền"),
                          autofocus: true
                        ))),
                Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: FlatButton(
                              color: Colors.blue,
                              child: Text("Gửi", style: TextStyle(color: Colors.white)),
                              onPressed: () {
                                print("Hóng!");
                              },
                            ))
                      ],
                    ))
              ],
            )));;
  }
}
