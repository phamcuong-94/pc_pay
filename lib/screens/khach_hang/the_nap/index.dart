import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pc_pay/scoped_models/app_model.dart';
import 'package:scoped_model/scoped_model.dart';
import '../../../widgets/CustomItemCard.dart';

class TheNapScreen extends StatefulWidget {
  final String title = "Thẻ nạp";
  final IconData icon = FontAwesomeIcons.creditCard;

  @override
  State<StatefulWidget> createState() {
    return TheNapScreenState();
  }
}

class TheNapScreenState extends State<TheNapScreen> {

  @override
  Widget build(BuildContext context) {
    Widget _listTheNap() {
      return ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          Card(
              child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                          flex: 2,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  CustomItemCard.itemPadding(text: "Số seri: 53835832354"),
                                  CustomItemCard.itemPadding(text: "Mã thẻ: 4522330866328"),
                                  CustomItemCard.itemPadding(text: "Mệnh giá: 50.000")
                                ],
                              )
                            ],
                          )),
                      Expanded(
                          flex: 1,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              GestureDetector(
                                child: Text("Đã thanh toán",
                                    style: TextStyle(color: Colors.green)),
                                onTap: () {
                                  print("Chưa có bán bạn ơi!");
                                },
                              )
                            ],
                          ))
                    ],
                  )))
        ],
      );
    }

    return ScopedModelDescendant<AppModel>(
      builder: (BuildContext context, Widget child, AppModel model) {
        return Center(
          child: _listTheNap(),
        );
      },
    );
  }
}
